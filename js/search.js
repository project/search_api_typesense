((Drupal, TypesenseInstantSearchAdapter, instantsearch) => {
  Drupal.behaviors.search = {
    attach(context, settings) {
      const [searchbox] = once('searchbox', '#searchbox', context);
      if (searchbox === undefined) {
        return;
      }

      const typesenseInstantsearchAdapter = new TypesenseInstantSearchAdapter({
        server: {
          apiKey: settings.search_api_typesense.api_key,
          nodes: [
            {
              host: settings.search_api_typesense.host,
              port: settings.search_api_typesense.port,
              protocol: settings.search_api_typesense.protocol,
            },
          ],
        },
        additionalSearchParameters: {
          query_by: settings.search_api_typesense.query_by_fields,
          query_by_weights: settings.search_api_typesense.query_by_weights,
          sort_by: settings.search_api_typesense.sort_by_fields,
          exclude_fields: 'embedding',
          exhaustive_search: true,
        },
      });
      const { searchClient } = typesenseInstantsearchAdapter;

      const search = instantsearch({
        searchClient,
        indexName: settings.search_api_typesense.index,
        routing: true,
      });

      search.addWidgets([
        instantsearch.widgets.configure({
          hitsPerPage: 12,
        }),
        instantsearch.widgets.searchBox({
          container: '#searchbox',
          placeholder: 'Search...',
        }),
        instantsearch.widgets.hits({
          container: '#hits',
          templates: {
            item(hit, { html, components }) {
              return html`
                <article>
                  ${settings.search_api_typesense.all_fields.map((field) => {
                    if (Array.isArray(hit[field])) {
                      return html`<p class="array-field"><strong>${field}</strong>:
                        <ul>
                          ${hit[field].map((value) => {
                            return html` <li>${value}</li>`;
                          })}
                        </ul></p>`;
                    }
                    return html`<p>
                      <strong>${field}</strong>:
                      ${components.Snippet({ hit, attribute: field })}
                    </p>`;
                  })}
                  <hr />
                  <p><strong>id</strong>: ${hit.id}</p>
                </article>
              `;
            },
          },
        }),
        instantsearch.widgets.pagination({
          container: '#pagination',
        }),
        instantsearch.widgets.stats({
          container: '#stats',
        }),
      ]);

      settings.search_api_typesense.facet_string_fields.forEach((facet) => {
        search.addWidgets([
          instantsearch.widgets.refinementList({
            container: `#${facet}`,
            attribute: facet,
            searchable: true,
          }),
        ]);
      });

      settings.search_api_typesense.facet_number_fields.forEach((facet) => {
        search.addWidgets([
          instantsearch.widgets.refinementList({
            container: `#${facet}`,
            attribute: facet,
            searchable: true,
          }),
        ]);
      });

      search.start();
    },
  };
})(Drupal, TypesenseInstantSearchAdapter, instantsearch); // eslint-disable-line
