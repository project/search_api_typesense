<?php

declare(strict_types=1);

namespace Drupal\search_api_typesense\Hook;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Defines a class containing Search API Typesense hooks.
 */
class SearchApiTypesenseHooks {

  use StringTranslationTrait;

  /**
   * Implements hook_help().
   */
  #[Hook('help')]
  public function help(string $route_name): string {
    switch ($route_name) {
      case 'search_api_typesense.collection.export':
        return '<p>' . $this->t(
            'Export and download the full collection data as a json file.',
          ) . '</p>';
    }

    return '';
  }

  /**
   * Implements hook_theme().
   */
  #[Hook('theme')]
  public function theme(): array {
    return [
      'search_api_typesense_search' => [
        'variables' => [
          'facet_number_fields' => [],
          'facet_string_fields' => [],
        ],
      ],
      'search_api_typesense_converse' => [
        'variables' => [
          'models' => [],
        ],
      ],
    ];
  }

  /**
   * Implements hook_form_alter().
   */
  #[Hook('form_alter')]
  public function formAlter(
    array &$form,
    FormStateInterface $form_state,
    string $form_id,
  ): void {
    if ($form_id == 'search_api_index_fields') {
      $form['warning'] = [
        '#markup' => '<div class="messages messages--warning"><h3>Warning</h3><p>After choosing or updating fields here, you must go to the <em>Schema</em> tab, review the configuration, and submit the form.</p></div>',
        '#weight' => -100,
      ];
    }
  }

}
