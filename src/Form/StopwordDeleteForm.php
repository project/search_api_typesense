<?php

declare(strict_types=1);

namespace Drupal\search_api_typesense\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\search_api\ServerInterface;
use Drupal\search_api_typesense\Api\SearchApiTypesenseException;
use Drupal\search_api_typesense\Api\TypesenseClientInterface;
use Drupal\search_api_typesense\LoggerTrait;
use Drupal\search_api_typesense\Plugin\search_api\backend\SearchApiTypesenseBackend;

/**
 * Form to delete a stopword.
 */
class StopwordDeleteForm extends ConfirmFormBase {

  use LoggerTrait;

  /**
   * The Typesense client.
   *
   * @var \Drupal\search_api_typesense\Api\TypesenseClientInterface
   */
  protected TypesenseClientInterface $typesenseClient;

  /**
   * The search API server.
   *
   * @var \Drupal\search_api\ServerInterface|null
   */
  private ?ServerInterface $searchApiServer;

  /**
   * The stopword name.
   *
   * @var string|null
   */
  private ?string $stopwordName;

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'search_api_typesense_stopword_delete';
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\search_api\SearchApiException
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
    ?ServerInterface $search_api_server = NULL,
    ?string $id = NULL,
  ): array {
    $backend = $search_api_server->getBackend();
    if (!$backend instanceof SearchApiTypesenseBackend) {
      throw new SearchApiTypesenseException('The server must use the Typesense backend.');
    }

    if (!$backend->isAvailable()) {
      $this->messenger()->addError(
        $this->t('The Typesense server is not available.'),
      );
    }

    $this->searchApiServer = $search_api_server;
    $this->stopwordName = $id;
    $this->typesenseClient = $backend->getTypesenseClient();

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(
    array &$form,
    FormStateInterface $form_state,
  ): void {
    try {
      $stopword = $this->typesenseClient->deleteStopword($this->stopwordName);

      $this->messenger()->addStatus($this->t('Stopword set %id has been deleted.', [
        '%id' => $stopword['id'],
      ]));
    }
    catch (SearchApiTypesenseException $e) {
      $this->logError(
        $e,
        $this->t('The stopword set could not be deleted'),
      );
    }

    $form_state->setRedirect('search_api_typesense.server.stopwords', [
      'search_api_server' => $this->searchApiServer->id(),
    ]);
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\search_api_typesense\Api\SearchApiTypesenseException
   */
  public function getQuestion(): TranslatableMarkup {
    $stopword = $this->typesenseClient->retrieveStopword($this->stopwordName);

    return $this->t('Do you want to delete stopword %id?', [
      '%id' => $stopword['stopwords']['id'],
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): Url {
    return Url::fromRoute('search_api_typesense.server.stopwords', [
      'search_api_server' => $this->searchApiServer->id(),
    ]);
  }

}
