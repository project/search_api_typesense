<?php

declare(strict_types=1);

namespace Drupal\search_api_typesense\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\search_api\IndexInterface;
use Drupal\search_api_typesense\AiModels;
use Drupal\search_api_typesense\Api\SearchApiTypesenseException;
use Drupal\search_api_typesense\Plugin\search_api\backend\SearchApiTypesenseBackend;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

/**
 * Controller for Typesense index operations.
 */
class TypesenseIndexController extends ControllerBase {

  /**
   * TypesenseIndexController constructor.
   *
   * @param \Drupal\search_api_typesense\AiModels $aiModels
   *   The AI models.
   */
  public function __construct(
    #[Autowire(service: 'search_api_typesense.ai_models')]
    private readonly AiModels $aiModels,
  ) {
  }

  /**
   * Try the search.
   *
   * @param \Drupal\search_api\IndexInterface $search_api_index
   *   The search index.
   *
   * @return array
   *   The render array.
   *
   * @throws \Drupal\search_api\SearchApiException
   */
  public function search(IndexInterface $search_api_index): array {
    $backend = $search_api_index->getServerInstance()->getBackend();
    if (!$backend instanceof SearchApiTypesenseBackend) {
      throw new SearchApiTypesenseException('The server must use the Typesense backend.');
    }

    if (!$backend->isAvailable()) {
      $this->messenger()->addError(
        $this->t('The Typesense server is not available.'),
      );

      return [];
    }

    $configuration = $backend->getConfiguration();
    $typesense_client = $backend->getTypesenseClient();
    $all_fields = $typesense_client->getFields($search_api_index->id());
    $query_by_fields = $typesense_client->getFieldsForQueryBy($search_api_index->id());
    $facet_number_fields = $typesense_client->getFieldsForFacetNumber($search_api_index->id());
    $facet_string_fields = $typesense_client->getFieldsForFacetString($search_api_index->id());
    $schema = $backend->getSchema($search_api_index->id())?->getSchema();
    $query_by_weights = $schema['fields'] ? $typesense_client->getQueryByWeight($schema['fields']) : '';
    $sort_by_fields = $typesense_client->getFieldsForSortBy($schema['fields']);

    $build = [];
    $build['content'] = [
      '#theme' => 'search_api_typesense_search',
      '#facet_number_fields' => $facet_number_fields,
      '#facet_string_fields' => $facet_string_fields,
      '#attached' => [
        'drupalSettings' => [
          'search_api_typesense' => [
            'api_key' => $configuration['admin_api_key'],
            'host' => $configuration['nodes'][0]['host'],
            'port' => $configuration['nodes'][0]['port'],
            'protocol' => $configuration['nodes'][0]['protocol'],
            'index' => $search_api_index->id(),
            'all_fields' => $all_fields,
            'query_by_fields' => \implode(',', $query_by_fields),
            'query_by_weights' => \implode(',', $query_by_weights),
            'sort_by_fields' => \implode(',', $sort_by_fields),
            'facet_number_fields' => $facet_number_fields,
            'facet_string_fields' => $facet_string_fields,
          ],
        ],
      ],
    ];

    return $build;
  }

  /**
   * Render the Converse page.
   *
   * @param \Drupal\search_api\IndexInterface $search_api_index
   *   The search index.
   *
   * @return string[]
   *   The render array.
   *
   * @throws \Drupal\search_api\SearchApiException
   * @throws \Drupal\search_api_typesense\Api\SearchApiTypesenseException
   */
  public function converse(IndexInterface $search_api_index): array {
    if (!$this->aiModels->isAiSupportAvailable()) {
      $this->messenger()->addError(
        $this->t(
          'No AI support available. Be sure to enable the <a href=":ai_module" target="_blank">AI module</a> and at least one supported AI provider.',
          [
            ':ai_module' => 'https://www.drupal.org/project/ai',
          ],
        ),
      );

      return [];
    }

    $backend = $search_api_index->getServerInstance()->getBackend();
    if (!$backend instanceof SearchApiTypesenseBackend) {
      throw new \InvalidArgumentException('The server must use the Typesense backend.');
    }

    if (!$backend->isAvailable()) {
      $this->messenger()->addError(
        $this->t('The Typesense server is not available.'),
      );

      return [];
    }

    $configuration = $backend->getConfiguration();
    $typesense_client = $backend->getTypesenseClient();

    $build = [
      '#theme' => 'search_api_typesense_converse',
      '#models' => $typesense_client->retrieveConversationModels(),
      '#attached' => [
        'drupalSettings' => [
          'search_api_typesense' => [
            'api_key' => $configuration['admin_api_key'],
            'url' => \sprintf(
              '%s://%s:%s',
              $configuration['nodes'][0]['protocol'],
              $configuration['nodes'][0]['host'],
              $configuration['nodes'][0]['port'],
            ),
            'index' => $search_api_index->id(),
          ],
        ],
      ],
    ];

    return $build;
  }

}
