<?php

declare(strict_types=1);

namespace Drupal\search_api_typesense\Api;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Http\Client\Exception;
use Typesense\Client;
use Typesense\Collection;
use Typesense\Exceptions\ConfigError;
use Typesense\Exceptions\ObjectNotFound;
use Typesense\Exceptions\TypesenseClientError;

/**
 * The Search Api Typesense client.
 */
class TypesenseClient implements TypesenseClientInterface {

  use StringTranslationTrait;

  const CONVERSATION_HISTORY_COLLECTION_NAME = 'conversation_store';

  private Client $client;

  /**
   * TypesenseClient constructor.
   *
   * @param \Drupal\search_api_typesense\Api\Config $config
   *   The Typesense config.
   *
   * @throws \Drupal\search_api_typesense\Api\SearchApiTypesenseException
   */
  public function __construct(Config $config) {
    try {
      $this->client = new Client($config->toArray());
    }
    catch (ConfigError $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function retrieveCollections(): array {
    try {
      return $this->client->collections->retrieve();
    }
    catch (Exception | TypesenseClientError $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function searchDocuments(
    string $collection_name,
    array $parameters,
  ): array {
    try {
      if ($collection_name == '' || $parameters == []) {
        return [];
      }

      $collection = $this->retrieveCollection($collection_name);

      return $collection->documents->search($parameters);
    }
    catch (Exception | TypesenseClientError $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createCollection(array $schema): Collection {
    try {
      $this->client->collections->create($schema);
    }
    catch (Exception | TypesenseClientError $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }

    return $this->retrieveCollection($schema['name']);
  }

  /**
   * {@inheritdoc}
   */
  public function dropCollection(?string $collection_name): void {
    try {
      $collections = $this->client->collections;
      if ($collections->offsetExists($collection_name)) {
        $collections[$collection_name]->delete();
      }
    }
    catch (Exception | TypesenseClientError $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createDocument(
    string $collection_name,
    array $document,
  ): void {
    try {
      $collection = $this->retrieveCollection($collection_name);
      $collection->documents->upsert($document);
    }
    catch (Exception | TypesenseClientError $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function retrieveDocument(
    string $collection_name,
    string $id,
  ): array | null {
    try {
      $collection = $this->retrieveCollection($collection_name);

      return $collection
        ->documents[$this->prepareId($id)]->retrieve();
    }
    catch (ObjectNotFound $e) {
      return NULL;
    }
    catch (Exception | TypesenseClientError $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteDocument(string $collection_name, string $id): array {
    try {
      $collection = $this->retrieveCollection($collection_name);

      $typesense_id = $this->prepareId($id);

      $document = $this->retrieveDocument($collection_name, $id);
      if ($document != NULL) {
        return $collection->documents[$typesense_id]->delete();
      }

      return [];
    }
    catch (Exception | TypesenseClientError $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteDocuments(
    string $collection_name,
    array $filter_condition,
  ): array {
    try {
      $collection = $this->retrieveCollection($collection_name);

      return $collection->documents->delete($filter_condition);
    }
    catch (Exception | TypesenseClientError $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createSynonym(
    string $collection_name,
    string $id,
    array $synonym,
  ): array {
    try {
      $collection = $this->retrieveCollection($collection_name);

      return $collection->synonyms->upsert($id, $synonym);
    }
    catch (Exception | TypesenseClientError $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function retrieveSynonym(string $collection_name, string $id): array {
    try {
      $collection = $this->retrieveCollection($collection_name);

      return $collection->synonyms[$id]->retrieve();
    }
    catch (ObjectNotFound $e) {
      throw new SearchApiTypesenseException(
        $this->t('Synonym not found.')->render(),
      );
    }
    catch (Exception | TypesenseClientError $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function retrieveSynonyms(string $collection_name): array {
    try {
      $collection = $this->retrieveCollection($collection_name);

      return $collection->synonyms->retrieve();
    }
    catch (Exception | TypesenseClientError $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteSynonym(string $collection_name, string $id): array {
    try {
      $collection = $this->retrieveCollection($collection_name);

      return $collection->synonyms[$id]->delete();
    }
    catch (Exception | TypesenseClientError $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createCuration(
    string $collection_name,
    string $id,
    array $curation,
  ): array {
    try {
      $collection = $this->retrieveCollection($collection_name);

      return $collection->overrides->upsert($id, $curation);
    }
    catch (Exception | TypesenseClientError $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function retrieveCuration(string $collection_name, string $id): array {
    try {
      $collection = $this->retrieveCollection($collection_name);

      return $collection->overrides[$id]->retrieve();
    }
    catch (ObjectNotFound $e) {
      throw new SearchApiTypesenseException(
        $this->t('Curation not found.')->render(),
      );
    }
    catch (Exception | TypesenseClientError $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function retrieveCurations(string $collection_name): array {
    try {
      $collection = $this->retrieveCollection($collection_name);

      return $collection->overrides->retrieve();
    }
    catch (Exception | TypesenseClientError $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteCuration(string $collection_name, string $id): array {
    try {
      $collection = $this->retrieveCollection($collection_name);

      return $collection->overrides[$id]->delete();
    }
    catch (Exception | TypesenseClientError $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createConversationModel(
    array $params,
  ): array {
    try {
      return $this->client->conversations->models->create($params);
    }
    catch (Exception | TypesenseClientError $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function retrieveConversationModel(string $id): array {
    try {
      return $this->client->conversations->models[$id]->retrieve();
    }
    catch (ObjectNotFound $e) {
      throw new SearchApiTypesenseException(
        $this->t('Conversation model not found.')->render(),
      );
    }
    catch (Exception | TypesenseClientError $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function retrieveConversationModels(): array {
    try {
      return $this->client->conversations->models->retrieve();
    }
    catch (ObjectNotFound $e) {
      return [];
    }
    catch (Exception | TypesenseClientError $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function updateConversationModel(string $id, array $params): array {
    try {
      return $this->client->conversations->models[$id]->update($params);
    }
    catch (Exception | TypesenseClientError $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteConversationModel(string $id): array {
    try {
      return $this->client->conversations->models[$id]->delete();
    }
    catch (Exception | TypesenseClientError $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function hasConversationHistoryCollection(): bool {
    return $this->retrieveCollection(
      TypesenseClient::CONVERSATION_HISTORY_COLLECTION_NAME,
      FALSE,
      ) !== NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function ensureConversationHistoryCollection(): void {
    try {
      $conversation_store = $this
        ->retrieveCollection(
          TypesenseClient::CONVERSATION_HISTORY_COLLECTION_NAME,
          FALSE,
        );

      if ($conversation_store == NULL) {
        $this->client->collections->create([
          'name' => 'conversation_store',
          'fields' => [
            [
              'name' => 'conversation_id',
              'type' => 'string',
            ],
            [
              'name' => 'model_id',
              'type' => 'string',
            ],
            [
              'name' => 'timestamp',
              'type' => 'int32',
            ],
            [
              'name' => 'role',
              'type' => 'string',
              'index' => FALSE,
            ],
            [
              'name' => 'message',
              'type' => 'string',
              'index' => FALSE,
            ],
          ],
        ]);
      }
    }
    catch (Exception | TypesenseClientError $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function retrieveCollectionInfo(
    string $collection_name,
  ): array | null {
    try {
      $collection = $this->retrieveCollection($collection_name, FALSE);

      if ($collection === NULL) {
        return NULL;
      }

      $collection_data = $collection->retrieve();

      return [
        'created_at' => $collection_data['created_at'],
        'num_documents' => $collection_data['num_documents'],
      ];
    }
    catch (Exception | TypesenseClientError $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function retrieveHealth(): array {
    try {
      return $this->client->health->retrieve();
    }
    catch (Exception | TypesenseClientError $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function retrieveDebug(): array {
    try {
      return $this->client->debug->retrieve();
    }
    catch (Exception | TypesenseClientError $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function retrieveMetrics(): array {
    try {
      return $this->client->metrics->retrieve();
    }
    catch (Exception | TypesenseClientError $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createKey(array $schema): array {
    try {
      return $this->client->keys->create($schema);
    }
    catch (Exception | TypesenseClientError $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function retrieveKey(int $key_id): array {
    try {
      $key = $this->client->keys[$key_id];

      return $key->retrieve();
    }
    catch (ObjectNotFound $e) {
      throw new SearchApiTypesenseException(
        $this->t('Key not found.')->render(),
      );
    }
    catch (Exception | TypesenseClientError $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function retrieveKeys(): array {
    try {
      return $this->client->getKeys()->retrieve();
    }
    catch (Exception | TypesenseClientError $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteKey(int $key_id): array {
    try {
      $key = $this->client->keys[$key_id];

      return $key->delete();
    }
    catch (Exception | TypesenseClientError $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createStopword(array $schema): array {
    try {
      return $this->client->stopwords->put($schema);
    }
    catch (Exception | TypesenseClientError $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function retrieveStopword(string $name): array {
    try {
      return $this->client->stopwords->get($name);
    }
    catch (ObjectNotFound $e) {
      throw new SearchApiTypesenseException(
        $this->t('Stopword set not found.')->render(),
      );
    }
    catch (Exception | TypesenseClientError $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function retrieveStopwords(): array {
    try {
      return $this->client->stopwords->getAll();
    }
    catch (Exception | TypesenseClientError $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteStopword(string $name): array {
    try {
      return $this->client->stopwords->delete($name);
    }
    catch (Exception | TypesenseClientError $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function importCollectionData(
    string $collection_name,
    array $data,
  ): void {
    try {
      $collection = $this->retrieveCollection($collection_name);

      foreach ($data['synonyms'] as $synonym) {
        $collection->synonyms->upsert($synonym['id'], $synonym);
      }

      foreach ($data['curations'] as $curation) {
        $collection->overrides->upsert($curation['id'], $curation);
      }
    }
    catch (Exception | TypesenseClientError $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function exportCollectionData(
    string $collection_name,
    bool $include_schema = FALSE,
  ): array {
    try {
      $collection = $this->retrieveCollection($collection_name);

      $data = [
        'synonyms' => $collection->synonyms->retrieve()['synonyms'],
        'curations' => $collection->overrides->retrieve()['overrides'],
      ];

      if ($include_schema) {
        $schema = $collection->retrieve();
        unset($schema['created_at']);
        unset($schema['num_documents']);

        $data['schema'] = $schema;
      }

      return $data;
    }
    catch (Exception | TypesenseClientError $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function prepareId(string $id): string {
    // TypeSense does not allow characters that require encoding in urls. The
    // Search API ID has "/" character in it that is not compatible with that
    // requirement. So replace that with an underscore.
    // @see https://typesense.org/docs/latest/api/documents.html#index-a-document.
    return \str_replace('/', '-', $id);
  }

  /**
   * {@inheritdoc}
   */
  public function prepareItemValue(
    array $value,
    string $type,
  ): bool | float | int | string | array {
    // In Drupal every field is represented as an array. If the field is an
    // array with only one value, then we can use that value as the field value.
    // Except for the TypeSense types that require an array value.
    if (!\str_contains($type, '[]')) {
      if (\count($value) > 1) {
        throw new SearchApiTypesenseException(
          $this->t(
            'The field type %type does not support multiple values.',
            ['%type' => $type],
          )->render(),
        );
      }
      else {
        $value = \reset($value);
      }
    }

    switch ($type) {
      case 'typesense_bool[]':
        if (!$value) {
          $value = [];
        }
        else {
          $v = [];
          foreach ($value as $item) {
            $v[] = (bool) $item;
          }
          $value = $v;
        }
        break;

      case 'typesense_float[]':
        if (!$value) {
          $value = [];
        }
        else {
          $v = [];
          foreach ($value as $item) {
            $v[] = (float) $item;
          }
          $value = $v;
        }
        break;

      case 'typesense_int32[]':
      case 'typesense_int64[]':
        if (!$value) {
          $value = [];
        }
        else {
          $v = [];
          foreach ($value as $item) {
            $v[] = (int) $item;
          }
          $value = $v;
        }
        break;

      case 'typesense_string[]':
        if (!$value) {
          $value = [];
        }
        else {
          $v = [];
          foreach ($value as $item) {
            $v[] = (string) $item;
          }
          $value = $v;
        }
        break;
    }

    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function getFields(string $collection_name): array {
    try {
      $collection = $this->retrieveCollection($collection_name);
      if ($collection === NULL) {
        return [];
      }

      $schema = $collection->retrieve();

      return \array_map(static function (array $field) {
        return $field['name'];
      }, $schema['fields']);
    }
    catch (Exception | TypesenseClientError | SearchApiTypesenseException $e) {
      return [];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldsForQueryBy(string $collection_name): array {
    try {
      $collection = $this->retrieveCollection($collection_name);
      if ($collection === NULL) {
        return [];
      }

      $schema = $collection->retrieve();

      return \array_map(static function (array $field) {
        return $field['name'];
      }, \array_filter($schema['fields'], static function ($field) {
        return \in_array($field['type'], ['string', 'string[]'], TRUE) && $field['index'];
      }));
    }
    catch (Exception | TypesenseClientError | SearchApiTypesenseException $e) {
      return [];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldsForFacetNumber(string $collection_name): array {
    try {
      $collection = $this->retrieveCollection($collection_name);
      if ($collection === NULL) {
        return [];
      }

      $schema = $collection->retrieve();

      return \array_values(\array_map(static function (array $field) {
        return $field['name'];
      }, \array_filter($schema['fields'], static function ($field) {
        return $field['facet'] == TRUE && \in_array($field['type'], [
          'int32',
          'int64',
          'float',
          'int32[]',
          'int64[]',
          'float[]',
        ], TRUE);
      })));
    }
    catch (Exception | TypesenseClientError | SearchApiTypesenseException $e) {
      return [];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldsForFacetString(string $collection_name): array {
    try {
      $collection = $this->retrieveCollection($collection_name);
      if ($collection === NULL) {
        return [];
      }

      $schema = $collection->retrieve();

      return \array_values(\array_map(static function (array $field) {
        return $field['name'];
      }, \array_filter($schema['fields'], static function ($field) {
        return $field['facet'] == TRUE && \in_array($field['type'], [
          'string',
          'string[]',
        ], TRUE);
      })));
    }
    catch (Exception | TypesenseClientError | SearchApiTypesenseException $e) {
      return [];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getQueryByWeight(array $fields): array {
    try {
      return \array_map(static function (array $field) {
        return $field['weight'];
      }, \array_filter($fields, static function ($field) {
        return \in_array($field['type'], ['string', 'string[]'], TRUE) && $field['index'];
      }));
    }
    catch (Exception | TypesenseClientError | SearchApiTypesenseException $e) {
      return [];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldsForSortBy(array $fields): array {
    try {
      $filteredFields = \array_filter($fields, static function ($field) {
        return isset($field['index'], $field['sort']) && $field['index'] && $field['sort'];
      });

      return \array_map(static function ($field) {
        return $field['name'] . ':' . $field['sort_type'];
      }, $filteredFields);
    }
    catch (Exception | TypesenseClientError | SearchApiTypesenseException $e) {
      return [];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function generateScopedSearchKey(
    string $key,
    array $parameters,
  ): string {
    try {
      return $this->client->keys->generateScopedSearchKey($key, $parameters);
    }
    catch (\JsonException $e) {
      throw new SearchApiTypesenseException(
        $e->getMessage(),
        $e->getCode(),
        $e,
      );
    }
  }

  /**
   * Gets a Typesense collection.
   *
   * @param string|null $collection_name
   *   The name of the collection to retrieve.
   * @param bool $throw
   *   Whether to throw an exception if the collection is not found.
   *
   * @return \Typesense\Collection|null
   *   The collection, or NULL if none was found.
   *
   * @throws \Drupal\search_api_typesense\Api\SearchApiTypesenseException
   *
   * @see https://typesense.org/docs/latest/api/collections.html#retrieve-a-collection
   */
  private function retrieveCollection(
    ?string $collection_name,
    bool $throw = TRUE,
  ): ?Collection {
    try {
      $collection = $this->client->collections[$collection_name];
      // Ensure that collection exists on the typesense server by retrieving it.
      // This throws exception if it is not found.
      $collection->retrieve();

      return $collection;
    }
    catch (Exception | TypesenseClientError $e) {
      if ($throw) {
        throw new SearchApiTypesenseException(
          $e->getMessage(),
          $e->getCode(),
          $e,
        );
      }

      return NULL;
    }
  }

}
