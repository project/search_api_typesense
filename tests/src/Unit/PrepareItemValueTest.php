<?php

declare(strict_types=1);

namespace Drupal\Tests\search_api_typesense\Unit;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\search_api_typesense\Api\Config;
use Drupal\search_api_typesense\Api\SearchApiTypesenseException;
use Drupal\search_api_typesense\Api\TypesenseClient;
use Drupal\Tests\UnitTestCase;
use GuzzleHttp\Client;

/**
 * Tests for the TypesenseClient class.
 *
 * @coversDefaultClass \Drupal\search_api_typesense\Api\TypesenseClient
 * @group search_api_typesense
 */
class PrepareItemValueTest extends UnitTestCase {

  /**
   * @var \Drupal\search_api_typesense\Api\TypesenseClient
   */
  private TypesenseClient $client;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $container = new ContainerBuilder();
    \Drupal::setContainer($container);

    $container->set('string_translation', $this->getStringTranslationStub());

    $http_client = $this->createMock(Client::class);
    $this->client = new TypesenseClient(
      new Config(
        api_key               : 'test',
        nearest_node          : NULL,
        nodes                 : [
          [
            'host' => '',
            'port' => '',
            'protocol' => '',
          ],
        ],
        retry_interval_seconds: 30,
        http_client           : $http_client,
      ),
    );
  }

  /**
   * Tests prepareItemValue method.
   *
   * @covers \Drupal\search_api_typesense\Api\TypesenseClient::prepareItemValue
   * @dataProvider valueProvider
   */
  public function testPrepareItemValue(
    array $value,
    string $type,
    bool | float | int | string | array | null $expected,
  ): void {
    if ($expected == NULL) {
      self::expectException(SearchApiTypesenseException::class);
    }

    $result = $this->client->prepareItemValue($value, $type);
    self::assertEquals($expected, $result);
  }

  /**
   * Tests prepareId method.
   *
   * @covers \Drupal\search_api_typesense\Api\TypesenseClient::prepareId
   */
  public function testPrepareId(): void {
    $result = $this->client->prepareId("entity/5");

    self::assertEquals("entity-5", $result);
  }

  /**
   * @dataProvider valueProvider
   */
  public static function valueProvider(): array {
    return [
      [[5], "typesense_string", "5"],
      [["5"], "typesense_string", "5"],
      [[TRUE], "typesense_string", "1"],
      [["5", "6"], "typesense_string[]", ["5", "6"]],
      [["5"], "typesense_int32", 5],
      [["5"], "typesense_float", 5.0],
      [["5"], "typesense_bool", TRUE],
      [["5", "6"], "typesense_int32", NULL],
    ];
  }

}
